package org.academiadecodigo.asynctomatics;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {

        Entity entity = new Entity();

        User user = new User();

        user.setId(1);
        user.setUsername("vniacsta");
        user.setPassword("nottellingyou1");
        user.setEmail("hello@vniacsta.com");
        user.setPhone(919191919);
        user.setVersion(0);
        user.setCreationTime(Timestamp.valueOf(LocalDateTime.now()));
        user.setUpdateTime(Timestamp.valueOf(LocalDateTime.now()));

        entity.saveOrUpdate(user);

        entity.fetch(user.getId());

        entity.close();
    }

}
