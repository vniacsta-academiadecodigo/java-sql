package org.academiadecodigo.asynctomatics;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;

public class Entity {

    private EntityManagerFactory emf;

    // constructor
    public Entity() {
        emf = Persistence.createEntityManagerFactory("user_info");
    }

    public User fetch(Integer id) {

        EntityManager em = emf.createEntityManager();

        try {
            return em.find(User.class, id);

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public User saveOrUpdate(User user) {

        EntityManager em = emf.createEntityManager();

        try {

            em.getTransaction().begin();

            User savedUser = em.merge(user);

            em.getTransaction().commit();

            return savedUser;

        } catch (RollbackException e) {
            em.getTransaction().rollback();
            return null;

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void close() {
        emf.close();
    }
}
