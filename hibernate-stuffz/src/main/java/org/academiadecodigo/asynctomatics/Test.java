package org.academiadecodigo.asynctomatics;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;

public class Test {

    private EntityManagerFactory emf;

    public Test() {
        // same name in the persistence.xml
        emf = Persistence.createEntityManagerFactory("user");
    }

    public static void main(String[] args) {

        Test test = new Test();

        User user = new User();
        user.setId(1);
        user.setName("Totó");
        user.setEmail("abreaporta@tania.com");
        user.setPhone(919191919);

        System.out.println("** Persisting user **");
        test.save(user);

        System.out.println(test.fetch(user.getId()));
    }

    private void save(User user) {

        EntityManager em = emf.createEntityManager();

        try {

            em.getTransaction().begin();
            em.persist(user);
            em.getTransaction().commit();

        } catch (RollbackException e) {
            em.getTransaction().rollback();

        } finally {
            em.close();
        }
    }

    private User fetch(Integer id) {

        EntityManager em = emf.createEntityManager();

        try {
            return em.find(User.class, id);

        } finally {
            em.close();
        }
    }
}
