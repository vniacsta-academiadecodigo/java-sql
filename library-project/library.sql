DROP DATABASE IF EXISTS library;
CREATE DATABASE library;
USE library;

CREATE TABLE books(
    book_id INTEGER AUTO_INCREMENT UNIQUE,
    book_title VARCHAR(80) NOT NULL,
    book_author VARCHAR(40) NOT NULL,
    book_year INTEGER(4) NOT NULL,
    book_publisher VARCHAR(40) NOT NULL,
    book_status BOOLEAN DEFAULT 0 NOT NULL,
    PRIMARY KEY (book_id)
);

CREATE TABLE users(
    user_id INTEGER AUTO_INCREMENT UNIQUE,
    user_name VARCHAR(20) NOT NULL,
    user_contact INTEGER(9) NOT NULL,
    user_city VARCHAR(20) NOT NULL,
    PRIMARY KEY (user_id)
);

CREATE TABLE reservations(
    reservation_id INTEGER AUTO_INCREMENT UNIQUE,
    book_id INTEGER,
    user_id INTEGER,
    day DATE,
    book_status BOOLEAN DEFAULT 0 NOT NULL,
    FOREIGN KEY (book_id) REFERENCES books(book_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE users_log(
    user_id INTEGER,
    book_id INTEGER,
    day date
);


INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Parábola do Cajado Velho", "Pepetela", 2010, "Dom Quixote");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Estorvo", "Chico Buarque", 2010, "Dom Quixote");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("A Varanda de Frangipani", "Mia Couto", 2010, "Dom Quixote");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("O Hóspede de Job", "José Cardoso Pires", 2010, "Dom Quixote");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Kikia Matcho", "Filinto de Barros", 2010, "Dom Quixote");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Os Dois Irmãos", "Germano Almeida", 2010, "Dom Quixote");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Fiodor Dostoievski", "A Submissa", 2010, "Arbor Literae");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Fiodor Dostoievski", "Noites Brancas", 2008, "Biblioteca Editores Independentes");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Fernando Pessoa", "Mensagem", 2007, "Biblioteca Editores Independentes");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("José Luís Peixoto", "Morreste-me", 2000, "Quetzal");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("José Luís Peixoto", "Antídoto", 2003, "Micrografia");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("José Luís Peixoto", "Uma Casa na Escuridão", 2009, "Quetzal");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("José Luís Peixoto", "Dentro do Segredo", 2012, "Quetzal");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("José Luís Peixoto", "Abraço", 2011, "Quetzal");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("José Luís Peixoto", "Livro", 2010, "Quetzal");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("José Luís Peixoto", "Cemitério de Pianos", 2009, "Quetzal");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("José Luís Peixoto", "Galveias", 2014, "Quetzal");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("José Luís Peixoto", "Cal", 2007, "Quetzal");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("José Saramago", "Evangelho Segundo Jesus Cristo", 2008, "Caminho");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Chanrithy Him", "When Broken Glass Floats", 2004, "WWNorton");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Fiodor Dostoievski", "O Jogador", 1965, "Progredior");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Raul Brandão", "Húmus", 2011, "Bertrand");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Camilo Castelo Branco", "A Queda de um Anjo", 2011, "Bertrand");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Eça de Queirós", "Contos", 2010, "Bertrand");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Eça de Queirós", "O Mandarim", 1985, "Lello & Irmão");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Florbela Espanca", "Sonetos", 1992, "Livraria Estante");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Franz Kafka", "O Processo", 2012, "Bertrand");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Anton Tchékhov", "Peças", 2006, "Relógio D'Água");
INSERT INTO books(book_title, book_author, book_year, book_publisher) VALUES ("Fiodor Dostoievski", "Crime e Castigo", 2001, "Presença");


INSERT INTO users(user_name, user_contact, user_city) VALUES ("Diogo", 911234567, "Porto");
INSERT INTO users(user_name, user_contact, user_city) VALUES ("Vânia", 915624189, "Senhora da Hora");
INSERT INTO users(user_name, user_contact, user_city) VALUES ("Hugo", 916324785, "Porto");
INSERT INTO users(user_name, user_contact, user_city) VALUES ("André", 916359663, "Leça da Palmeira");
INSERT INTO users(user_name, user_contact, user_city) VALUES ("Luís", 912236537, "Vila Nova de Gaia");
INSERT INTO users(user_name, user_contact, user_city) VALUES ("João", 965484852, "Porto");
INSERT INTO users(user_name, user_contact, user_city) VALUES ("José", 936665214, "Guimarães");
INSERT INTO users(user_name, user_contact, user_city) VALUES ("Emanuel", 963321478, "Braga");
INSERT INTO users(user_name, user_contact, user_city) VALUES ("Fábio", 914452366, "Porto");
INSERT INTO users(user_name, user_contact, user_city) VALUES ("Paulo", 937789584, "Braga");
INSERT INTO users(user_name, user_contact, user_city) VALUES ("Sara", 936658874, "Freamunde");
INSERT INTO users(user_name, user_contact, user_city) VALUES ("Vando", 936699887, "Ericeira");
INSERT INTO users(user_name, user_contact, user_city) VALUES ("Ricardo", 963332171, "Lisboa");



DELIMITER //
    CREATE TRIGGER change_book_status
    AFTER INSERT ON reservations
    FOR EACH ROW
    BEGIN
        UPDATE books
        SET books.book_status = NEW.book_status
        WHERE book_id = NEW.book_id;
    END //


    CREATE PROCEDURE get_book_by_author(IN author VARCHAR(255))
    BEGIN
        SELECT book_author, book_title FROM books
        WHERE books.book_author = author;
    END //

    
    CREATE PROCEDURE count_books_on_loan()
    BEGIN
        SELECT COUNT(*) AS Loans
        FROM books
        WHERE book_status > 0;
    END //

    
    CREATE PROCEDURE get_publisher_by_title(IN title VARCHAR(255))
    BEGIN
        SELECT book_title, book_publisher FROM books
        WHERE books.book_title = title;
    END // 
    

    CREATE PROCEDURE get_users_borrowed_books_published_before_date(IN year INTEGER(4))
    BEGIN
        SELECT user_name FROM users
        WHERE users.user_id IN(
            SELECT reservations.user_id
            FROM reservations
            WHERE reservations.book_id IN (
                SELECT books.book_id
                FROM books
                WHERE books.book_status > 0 AND books.book_year < year
                )
        );
    END //

DELIMITER ;


INSERT INTO reservations(book_id, user_id, day, book_status) VALUES (10, 7, CURDATE(), 1);
INSERT INTO reservations(book_id, user_id, day, book_status) VALUES (1, 2, CURDATE(), 1);
INSERT INTO reservations(book_id, user_id, day, book_status) VALUES (3, 5, CURDATE(), 1);
INSERT INTO reservations(book_id, user_id, day, book_status) VALUES (20, 9, CURDATE(), 1);
INSERT INTO reservations(book_id, user_id, day, book_status) VALUES (25, 11, CURDATE(), 1);
INSERT INTO reservations(book_id, user_id, day, book_status) VALUES (4, 1, CURDATE(), 1);
