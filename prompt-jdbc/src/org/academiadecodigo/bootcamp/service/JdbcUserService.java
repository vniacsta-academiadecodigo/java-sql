package org.academiadecodigo.bootcamp.service;

import org.academiadecodigo.bootcamp.model.User;
import org.academiadecodigo.bootcamp.persistence.ConnectionManager;
import org.academiadecodigo.bootcamp.utils.Security;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class JdbcUserService implements UserService {

    private ConnectionManager connectionManager;
    private Connection connection;

    public JdbcUserService(ConnectionManager connectionManager) {

        this.connectionManager = connectionManager;
        connection = connectionManager.getConnection();
    }

    @Override
    public boolean authenticate(String username, String password) {

        try {
            String query = "SELECT username, password FROM user " +
                    "WHERE username = ? AND password = ?;";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, username);
            statement.setString(2, Security.getHash(password));

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return true;
            }

            statement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public void add(User user) {

        try {
            String query = "INSERT INTO user " +
                    "(username, password, email, firstname, lastname, phone) " +
                    "VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getPhone());

            statement.execute();

            statement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public User findByName(String username) {

        User user = null;

        try {

            String query = "SELECT * FROM user WHERE username = ?;";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, username);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {

                String emailEl = resultSet.getString("email");
                String passwordEl = resultSet.getString("password");
                String firstNameEl = resultSet.getString("firstname");
                String lastNameEl = resultSet.getString("lastname");
                String phoneEl = resultSet.getString("phone");

                user = new User(username, emailEl, passwordEl, firstNameEl,
                        lastNameEl, phoneEl);
            }

            statement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return user;
    }

    @Override
    public List<User> findAll() {

        List<User> list = new LinkedList<>();

        try {
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM user;";

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {

                String usernameEl = resultSet.getString("username");
                String emailEl = resultSet.getString("email");
                String passwordEl = resultSet.getString("password");
                String firstNameEl = resultSet.getString("firstname");
                String lastNameEl = resultSet.getString("lastname");
                String phoneEl = resultSet.getString("phone");

                list.add(new User(usernameEl, emailEl, passwordEl, firstNameEl,
                        lastNameEl, phoneEl));
            }

            statement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return list;
    }

    @Override
    public int count() {

        int result = 0;

        try {
            Statement statement = connection.createStatement();

            String query = "SELECT COUNT(*) FROM user";

            ResultSet resultSet = statement.executeQuery(query);

            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }

            statement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;
    }
}
