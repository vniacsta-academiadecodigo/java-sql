DELIMITER //

CREATE PROCEDURE get_quests_completed_by_character
(IN param_c_id INT)

BEGIN

  SELECT c.name AS character_name, q.name 
    AS quest_name, j.is_completed AS is_completed 
    FROM characters AS c
  
  JOIN quests AS q
  
  LEFT JOIN journal AS j
    ON j.character_id = param_c_id
    AND c.character_id = param_c_id
    AND j.quest_id = q.quest_id

  WHERE j.is_completed = 'yes';

END //

DELIMITER ;
