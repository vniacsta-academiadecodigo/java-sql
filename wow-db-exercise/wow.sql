DROP DATABASE IF EXISTS wow;

CREATE DATABASE wow;
USE wow;

CREATE TABLE characters (
  character_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(40) NOT NULL,
  gender VARCHAR(10) NOT NULL,
  race VARCHAR(15) NOT NULL,
  class VARCHAR(15) NOT NULL,
  current_xp INT NOT NULL DEFAULT 0,
  PRIMARY KEY (character_id)
);

CREATE TABLE quests (
  quest_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  xp_reward INT NOT NULL,
  PRIMARY KEY (quest_id)
);

CREATE TABLE journal (
  character_id INT,
  quest_id INT,
  is_completed VARCHAR(5),
  xp_reward INT,
  PRIMARY KEY (character_id, quest_id),
  FOREIGN KEY (character_id) REFERENCES characters (character_id)
    ON DELETE CASCADE, 
  FOREIGN KEY (quest_id) REFERENCES quests (quest_id)
    ON DELETE NO ACTION
);


INSERT INTO characters VALUES (1, 'Vania', 'Female', 'Elf', 'Druid', 0);
INSERT INTO characters VALUES (2, 'Emanuel', 'Male', 'Gnome', 'Mage', 0);
INSERT INTO characters VALUES (3, 'Sara', 'Female', 'Human', 'Priest', 0);
INSERT INTO characters VALUES (4, 'JB', 'Male', 'Tauren', 'Warrior', 0);
INSERT INTO characters VALUES (5, 'Vando', 'Male', 'Orc', 'Warrior', 0);
INSERT INTO characters VALUES (6, 'Ana', 'Female', 'Tauren', 'Druid', 0);
INSERT INTO characters VALUES (7, 'Fabio', 'Male', 'Elf', 'Priest', 0);
INSERT INTO characters VALUES (8, 'Margarida', 'Female', 'Human', 'Warrior', 0);

INSERT INTO quests (name, xp_reward) VALUES ('Consecrated Letter', 50);
INSERT INTO quests (name, xp_reward) VALUES ('Give Gerard a Drink', 50);
INSERT INTO quests (name, xp_reward) VALUES ('A Threat Within', 50);
INSERT INTO quests (name, xp_reward) VALUES ('Kobold Camp Cleanup', 75);
INSERT INTO quests (name, xp_reward) VALUES ('Eagan Peltskinner', 75);
INSERT INTO quests (name, xp_reward) VALUES ('Wolves Across the Border', 100);
INSERT INTO quests (name, xp_reward) VALUES ('The Stolen Tome', 100);
INSERT INTO quests (name, xp_reward) VALUES ('In Favor of the Light', 100);
INSERT INTO quests (name, xp_reward) VALUES ('The Balance of Nature', 50);
INSERT INTO quests (name, xp_reward) VALUES ('Fel Moss Corruption', 50);
INSERT INTO quests (name, xp_reward) VALUES ('A Favor for Melithar', 75);
INSERT INTO quests (name, xp_reward) VALUES ('Signs of Things to Come', 75);
INSERT INTO quests (name, xp_reward) VALUES ('Webwood Corruption', 75);
INSERT INTO quests (name, xp_reward) VALUES ('Vile Touch', 100);
INSERT INTO quests (name, xp_reward) VALUES ('Dwarven Outfitters', 25);
INSERT INTO quests (name, xp_reward) VALUES ('A New Threat', 50);
INSERT INTO quests (name, xp_reward) VALUES ('Coldridge Valley Mail Delivery', 50);
INSERT INTO quests (name, xp_reward) VALUES ('The Boar Hunter', 75);
INSERT INTO quests (name, xp_reward) VALUES ('The Troll Cave', 100);
INSERT INTO quests (name, xp_reward) VALUES ('The Stolen Journal', 125);

SELECT * FROM characters;
