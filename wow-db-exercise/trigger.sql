DELIMITER //

CREATE TRIGGER update_xp 
  
  AFTER INSERT ON journal
  FOR EACH ROW

BEGIN

  UPDATE characters AS c, (
    SELECT character_id, SUM(xp_reward) AS xp_sum FROM journal
    GROUP BY character_id) AS j

  SET c.current_xp = j.xp_sum

  WHERE c.character_id = j.character_id;

END //

DELIMITER ;

